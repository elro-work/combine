//
//  PasswordState.swift
//  combine
//
//  Created by Patrik Duksin on 11.09.2021.
//

enum PasswordState: String {
    case valid = "✅"
    case empty = "Password mustn't be empty"
    case noMatch = "Passwords don't match"
    case short = "Password must be longer than 8 charachters"
    case notContainsSpecialCharacters = "Password must contains special charachter"
    case notContainsUppercasedCharacters = "Password must contain uppercased charachter"
}
