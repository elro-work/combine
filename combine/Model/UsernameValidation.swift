//
//  UsernameValidation.swift
//  combine
//
//  Created by Patrik Duksin on 11.09.2021.
//

enum UsernameState: String {
    case valid = "✅"
    case short = "Username must be longer than 4 characters"
}
