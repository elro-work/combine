//
//  String.swift
//  combine
//
//  Created by Patrik Duksin on 11.09.2021.
//

import Foundation

extension String {

    func hasUppercasedCharacters() -> Bool {
        return stringFulfillsRegex(
            regex: ".*[A-Z].*"
        )
    }

    func hasSpecialCharacters() -> Bool {
        return stringFulfillsRegex(
            regex: ".*[!@#$%^&*()].*"
        )
    }

    private func stringFulfillsRegex(regex: String) -> Bool {
        return NSPredicate(format: "SELF MATCHES %@", regex).evaluate(with: self)
    }
}
