//
//  ContentView.swift
//  combine
//
//  Created by Patrik Duksin on 09.09.2021.
//

import SwiftUI

struct ContentView: View {

    @State var presentAlert = false
    @ObservedObject private var userViewModel = UserViewModel()

    var body: some View {
        Form {
            Section(footer: Text(userViewModel.usernameMessage).foregroundColor(.red)) {
                TextField("Username", text: $userViewModel.username)
                    .autocapitalization(.none)
            }
            Section(footer: Text(userViewModel.passwordMessage).foregroundColor(.red)) {
                SecureField("Password", text: $userViewModel.password)
                SecureField("Password again", text: $userViewModel.passwordAgain)
            }
            Section {
                Button(action: self.signUp) {
                    Text("Sign up")
                }.disabled(!self.userViewModel.isValid)
            }
        }
        .sheet(isPresented: $presentAlert) {
            WelcomeView()
        }
    }

    private func signUp() {
        self.presentAlert = true
    }
}

struct WelcomeView: View {
    var body: some View {
        Text("Welcome! Great to have you on board!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
