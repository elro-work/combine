//
//  combineApp.swift
//  combine
//
//  Created by Patrik Duksin on 09.09.2021.
//

import SwiftUI

@main
struct CombineApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
