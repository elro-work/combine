//
//  UserViewModel.swift
//  combine
//
//  Created by Patrik Duksin on 11.09.2021.
//

import Foundation
import Combine

class UserViewModel: ObservableObject {

    // MARK: - Public properties
    @Published var username        = ""
    @Published var password        = ""
    @Published var passwordAgain   = ""
    @Published var usernameMessage = ""
    @Published var passwordMessage = ""
    @Published var isValid         = false

    // MARK: - Private properties
    private var cancellableSet: Set<AnyCancellable> = []

    private var isUsernameValidPublisher: AnyPublisher<UsernameState, Never> {
        $username
            .debounce(for: 0.8, scheduler: RunLoop.main)
            .removeDuplicates()
            .map { input in
                if input.count >= 4 {
                    return .valid
                }
                return .short
            }
            .eraseToAnyPublisher()
    }

    private var isPasswordEmptyPublisher: AnyPublisher<Bool, Never> {
        $password
            .debounce(for: 0.8, scheduler: RunLoop.main)
            .removeDuplicates()
            .map { password in
                return password == ""
            }
            .eraseToAnyPublisher()
    }

    private var arePasswordsEqualPublisher: AnyPublisher<Bool, Never> {
        Publishers.CombineLatest($password, $passwordAgain)
            .debounce(for: 0.2, scheduler: RunLoop.main)
            .map { password, passwordAgain in
                return password == passwordAgain
            }
            .eraseToAnyPublisher()
    }

    private var isPasswordStrongEnoughPublisher: AnyPublisher<PasswordState, Never> {
        $password
            .debounce(for: 0.2, scheduler: RunLoop.main)
            .removeDuplicates()
            .map { input in
                if input.hasSpecialCharacters() == false {
                    return .notContainsSpecialCharacters
                } else if input.hasUppercasedCharacters() == false {
                    return .notContainsUppercasedCharacters
                } else if input.count <= 8 {
                    return .short
                } else {
                    return .valid
                }
            }
            .eraseToAnyPublisher()
    }

    private var isPasswordValidPublisher: AnyPublisher<PasswordState, Never> {
        Publishers.CombineLatest3(isPasswordEmptyPublisher, arePasswordsEqualPublisher, isPasswordStrongEnoughPublisher)
            .map { passwordIsEmpty, passwordsAreEqual, passwordIsStrongEnough in
                if passwordIsEmpty {
                    return .empty
                } else if passwordsAreEqual == false {
                    return .noMatch
                } else {
                    return passwordIsStrongEnough
                }
            }
            .eraseToAnyPublisher()
    }

    private var isFormValidPublisher: AnyPublisher<Bool, Never> {
        Publishers.CombineLatest(isUsernameValidPublisher, isPasswordValidPublisher)
            .map { userNameIsValid, passwordIsValid in
                return (userNameIsValid == .valid) && (passwordIsValid == .valid)
            }
            .eraseToAnyPublisher()
    }

    // MARK: - Public methods
    init() {
        isUsernameValidPublisher
            .receive(on: RunLoop.main)
            .map { return $0.rawValue }
            .assign(to: \.usernameMessage, on: self)
            .store(in: &cancellableSet)

        isPasswordValidPublisher
            .receive(on: RunLoop.main)
            .map { return $0.rawValue }
            .assign(to: \.passwordMessage, on: self)
            .store(in: &cancellableSet)

        isFormValidPublisher
            .receive(on: RunLoop.main)
            .assign(to: \.isValid, on: self)
            .store(in: &cancellableSet)
    }
}
